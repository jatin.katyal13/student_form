# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-13 17:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0002_student_ci'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fee',
            name='course',
        ),
        migrations.AddField(
            model_name='fee',
            name='remarks',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='fee',
            name='student',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='form.Student'),
            preserve_default=False,
        ),
    ]
